<?php
session_start();
if(extension_loaded('zlib')){ob_start('ob_gzhandler');}
include 'app/inc/system.php';
include 'app/design/'.DESIGN.'/tpl/header.php'; 
debug('top');
$privateisnull = (isset($_SESSION['article'])) ? '' : 'AND private = "" ';
if(isset($_GET['q']) && $_GET['q'] != NULL) {
	$query = htmlspecialchars($_GET['q']); 
	if (preg_match('#^\s?"[^"]*"\s?$#', $query)) { // exact match
		$wordsearch = array(str_replace('"', '', $query));
	}
	else {
		$wordsearch=explode(' ',$query); 
	}
//on créée une variable
	foreach($wordsearch as $query) {
		@$where.='title LIKE :query OR ';
		@$where.='content LIKE :query OR ';
	}
	$where=substr($where, 0,-4); 
	if(SGBD == 'mysql') {
		$something = $bdd->prepare("SELECT * FROM ".PREFIX."news WHERE (".$where.") ".$privateisnull." ORDER BY timestamp DESC");
		$something->execute(array('query'=>'%'.$query.'%'));
		$nb_result = $something->rowCount(); 
		debug('query');
	}	
	if(SGBD == 'sqlite') {
		$something = $bdd->prepare("SELECT * FROM ".PREFIX."news WHERE (".$where.") ".$privateisnull." ORDER BY timestamp DESC");
		$something->execute(array('query'=>'%'.$query.'%'));
		$count = $bdd->prepare('SELECT count(*) AS nbr FROM '.PREFIX.'news WHERE ('.$where.') '.$privateisnull);
		$count->execute(array('query'=>'%'.$query.'%'));
		$result = $count->fetchAll();
		$nb_result = $result[0]['nbr'];
		debug('query');
	}	
}
if(isset($_GET['author']) && $_GET['author'] != NULL) {
	$query = intval($_GET['author']); 
	if(SGBD == 'mysql') {
		$something = $bdd->query("SELECT * FROM ".PREFIX."news WHERE author_id='".$query."' ".$privateisnull." ORDER BY timestamp DESC");
		$nb_result = $something->rowCount(); 
		debug('query');
	}	
	if(SGBD == 'sqlite') {
		$something = $bdd->query('SELECT * FROM '.PREFIX.'news WHERE author_id="'.$query.'" '.$privateisnull.' ORDER BY timestamp DESC');
		$count = $bdd->query('SELECT count(*) AS nbr FROM '.PREFIX.'news WHERE author_id="'.$query.'"');
		$result = $count->fetchAll();
		$nb_result = $result[0]['nbr'];
		debug('query');
	}	
}

if(isset($_GET['tag']) && $_GET['tag'] != NULL) {
	$query = htmlspecialchars($_GET['tag']); 
	$wordsearch=explode(TAG_SEPARATOR,$query); 
//on créée une variable
	foreach($wordsearch as $query) {
		@$where.='tag LIKE :query OR ';
	}
	$where=substr($where, 0,-4); 
	if(SGBD == 'mysql') {
		$something = $bdd->prepare("SELECT * FROM ".PREFIX."news WHERE (".$where.") ".$privateisnull." ORDER BY timestamp DESC");
		$something->execute(array('query'=>'%'.$query.'%'));
		$nb_result = $something->rowCount(); 
		debug('query');
	}	
	if(SGBD == 'sqlite') {
		$something = $bdd->prepare("SELECT * FROM ".PREFIX."news WHERE (".$where.") ".$privateisnull." ORDER BY timestamp DESC");
		$something->execute(array('query'=>'%'.$query.'%'));
		debug('query');
		$count = $bdd->prepare('SELECT count(*) AS nbr FROM '.PREFIX.'news WHERE ('.$where.') '.$privateisnull);
		$count->execute(array('query'=>'%'.$query.'%'));
		$result = $count->fetchAll();
		$nb_result = $result[0]['nbr'];
		debug('query');
	}	
}
if($nb_result != 0) {
	while($data = $something->fetch()) {
		if(time() >= $data['timestamp'] and $data['draft'] == 0) {
			echo '<a href="'.url_format($data['id'], FORMAT_URL_POST).'">'.$data['title'].'</a><br/>'.PHP_EOL;
		}
	} 
}
else {
	echo translate('notfound');
}
$form = New form(array('method'=>'get', 'action'=>'search.php'));
$form->input(array('type'=>'text', 'name'=>'q'));
$form->input(array('type'=>'submit'));	
$form->endform();
include 'app/design/'.DESIGN.'/tpl/footer.php';
debug('bottom');
if(DEBUG == true) {var_dump(get_defined_vars());}
?>