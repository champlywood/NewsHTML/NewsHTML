<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Installation</title>
<style type="text/css">
label+input {display:block}
</style>
</head>
<body>
<?php
define('DEBUG', true);
define('TIMEZONE', 'UTC');
define('LANG', 'en');
define('CAPTCHA', '');
define('SET_LOCALE', '');
include 'app/inc/system.php';
if(!is_dir('app/inc/lib')) {
	mkdir('app/inc/lib', 0777);
	if(!file_exists('app/inc/lib/Parsedown.php')) {
			file_put_contents('app/inc/lib/Parsedown.php', file_get_contents('https://raw.githubusercontent.com/erusev/parsedown/master/Parsedown.php'));
	}
	if(!file_exists('app/inc/lib/ParsedownExtra.php')) {
		file_put_contents('app/inc/lib/ParsedownExtra.php', file_get_contents('https://raw.githubusercontent.com/erusev/parsedown-extra/master/ParsedownExtra.php'));
	}
	if(!file_exists('app/inc/lib/pubsubhubbubpublisher.php')) {
		file_put_contents('app/inc/lib/pubsubhubbubpublisher.php', file_get_contents('https://raw.githubusercontent.com/pubsubhubbub/php-publisher/master/library/Publisher.php'));
	}
	if(!file_exists('app/inc/atom.class.php')) {
		file_put_contents('app/inc/atom.class.php', file_get_contents('http://champlywood.free.fr/bidouilles/dl/atom.class.phps'));
	}
	if(!file_exists('app/inc/form.class.php')) {
		file_put_contents('app/inc/form.class.php', file_get_contents('http://champlywood.free.fr/bidouilles/dl/form.class.phps'));
	}
	echo '<script>document.location.reload(true);</script>';
}
if(isset($_POST['submit'])) { 
	mkdir('data', 0777);			
	mkdir('data/upload', 0777);			
	file_put_contents('data/upload/upload.txt', '0');
	$salt =md5(uniqid(rand(), true));
	$pass = hash("sha512", $_POST['pass'].$salt); 
	$prefix = $_POST['prefix'];
	$texte = '<?php
	define("URL_DB", "'.trim($_POST['hote']).'");
	define("LOGIN", "'.trim($_POST['login']).'");
	define("PASSWORD", "'.trim($_POST['mdp']).'");
	define("TABLE", "'.trim($_POST['base']).'");
	define("SGBD", "'.trim($_POST['sgbd']).'");
	define("LANG", "'.trim($_POST['lang']).'");
	define("SET_LOCALE", "");
	define("DESIGN", "default");
	define("EMAIL", "no-reply@example.org");
	define("SALT", "'.$salt .'");
	define("TITLE", "Welcome in your blog !");
	define("DESCRIPTION", "You can change the title in your administration");
	define("ROOT", "'. trim($_POST['root']) .'");
	define("FORMAT_URL_POST", "/index.php?id=%d");
	define("FORMAT_URL_AUTHOR", "/search.php?author=%s");
	define("FORMAT_URL_TAG", "/search.php?tag=%s");
	define("FORMAT_URL_PAGINATION", "/?page=%d");
	define("FORMAT_PARSE", "markdown");
	define("TIMEZONE", "'.$_POST['timezone'].'");
	define("FORMAT_DATE", "dd/mm/yyyy");
	define("DEBUG", false);
	define("ENABLE_CACHE", true);
	$comment = true;
	define("MODERATION", false);
	define("PREFIX", "'.$prefix.'");
	define("NB_MESSAGES_PAGE", 10);
	define("TEXT_RESUME", false);
	define("TEXT_RESUME_NB", 100);
	define("TAG_SEPARATOR", " ");
	define("CAPTCHA", "");
	define("DEMO", false);
	?>';
	file_put_contents('data/config.php', $texte);
	if($_POST['sgbd'] =="sqlite") {
		try {
			$bdd = new PDO('sqlite:data/data.sqlite');
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} 
			catch (PDOException $e) {
			echo "<p>Error : " . $e->getMessage() . "</p>";
			exit();
		}
		$bdd->query("CREATE TABLE ".$prefix."news (
		id INTEGER PRIMARY KEY,
		link text,
		title text NOT NULL,
		content text NOT NULL,
		tag text NOT NULL,
		affcomment text NOT NULL,
		draft text NOT NULL,
		private text,
		author_id int(20) NOT NULL,
		timestamp int(20) NOT NULL default '0');");
		$bdd->query("CREATE TABLE ".$prefix."author (
		id_author INTEGER PRIMARY KEY,
		pseudo text NOT NULL,
		name text NOT NULL,
		password text NOT NULL,
		privilege int(20) NOT NULL);");
		$bdd->query("CREATE TABLE ".$prefix."comment (
		id INTEGER PRIMARY KEY,
		pseudo text NOT NULL,
		email text NOT NULL,
		website text NOT NULL,
		notif text NOT NULL,
		message text NOT NULL,
		idnews int(11) NOT NULL,
		moderation int(11) NOT NULL,
		timestamp bigint(20) NOT NULL);");
	$bdd->query("INSERT INTO ".$prefix."comment (pseudo, email, website, notif, message, idnews, moderation, timestamp) VALUES
	('example', 'example@example.org', 'http://example.org', '0', 'My first comment', 1, 1, 0);");
	$bdd->query("INSERT INTO ".$prefix."news ( title, content, tag, affcomment, draft, timestamp, author_id) VALUES ('my first article', 'edit me', 'edit me please', '1', '0', 0, 1);");
	$bdd->query("INSERT INTO ".$prefix."author (pseudo, name, password, privilege) VALUES ('".$_POST['user']."','".$_POST['user']."', '".$pass."', 1 );");
	}
	if($_POST['sgbd'] == "mysql") {
		try {
			$bdd = new PDO('mysql:host='.trim($_POST['hote']).';dbname='.trim($_POST['base']), trim($_POST['login']), trim($_POST['mdp']));
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} 
		catch (PDOException $e) {
			echo "<p>Erreur : " . $e->getMessage() . "</p>";
			exit();
		}
		$bdd->query("CREATE TABLE IF NOT EXISTS `".$prefix."news` (
		`id` int(11) NOT NULL auto_increment,
		`link` text character set utf8 NOT NULL,
		`title` varchar(255) character set utf8 NOT NULL,
		`content` text character set utf8 NOT NULL,
		`tag` text character set utf8 NOT NULL,
		`affcomment` varchar(255) NOT NULL,
		`draft` varchar(255) NOT NULL,
		`private` text,
		`author_id` int(11) NOT NULL,
		`timestamp` bigint(20) NOT NULL default '0',
		PRIMARY KEY (`id`)
		)ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;");
	$bdd->query("CREATE TABLE IF NOT EXISTS `".$prefix."author` (
		`id_author` int(11) NOT NULL auto_increment,
		`pseudo` varchar(255) character set utf8 NOT NULL,
		`name` varchar(255) character set utf8 NOT NULL,
		`password` text character set utf8 NOT NULL,
		`privilege` int(11)NOT NULL,
		PRIMARY KEY (`id_author`)
		)ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;");
		$bdd->query("CREATE TABLE IF NOT EXISTS `".$prefix."comment` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`pseudo` varchar(255) character set utf8 NOT NULL,
		`email` varchar(255) character set utf8 NOT NULL,
		`website` varchar(255) character set utf8 NOT NULL,
		`notif` varchar(255) NOT NULL,
		`message` text character set utf8 NOT NULL,
		`idnews` int(11) NOT NULL,
		`moderation` int(11) NOT NULL,
		`timestamp` bigint(20) NOT NULL,
		PRIMARY KEY (`id`),
		KEY `id` (`id`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
		INSERT INTO `".$prefix."comment` (`pseudo`, `email`, `website`, `notif`, `message`, `idnews`, `moderation`, `timestamp`) VALUES
		('example', 'example@example.org', 'http://example.org', '0', 'My first comment', '1', '1', 0);
		INSERT INTO `".$prefix."news` (`title`, `content`, `tag`, `affcomment`, `draft`, `timestamp`) VALUES ('my first article', 'edit me', 'edit me please', 1, 0, 0);
		INSERT INTO ".$prefix."author (`pseudo`, `name`, `password`, `privilege`) VALUES ('".$_POST['user']."','".$_POST['user']."', '".$pass."', 1 );
		");
	}
	echo 'Installation: Check';	
	echo '<br /><span class="note">Your news system is installed.</span><br /><a href="admin.php">Back to the administration</a>';	
	@unlink("install.php");
}
else {
	echo '<h1>Installation of NewsHTML</h1>';
	check_extension('pdo_mysql');
	check_extension('pdo_sqlite');
	check_extension('zlib');
	$array_pdo = array();
	if(extension_loaded('pdo_mysql')) {$array_pdo['mysql'] = 'MySQL';}
	if(extension_loaded('pdo_sqlite')) {$array_pdo['sqlite'] = 'SQLite';}
	$form = New form(array('method'=>'post', 'action'=>'install.php'));
	$form->startfieldset();
	$form->legend('SQL connexions');
	$form->label('sgbd', 'Type of database:');
	$form->select(array('name'=>'sgbd', 'id'=>'sgbd', 'onchange'=>'show_mysql_form()'), $array_pdo, 'sqlite', true);
	echo '<div id="mysql_vars" style="display:none;">';
	$form->label('hote', 'Host: ');
	$form->input(array('type'=>'text', 'name'=>'hote'));
	$form->label('login', 'User: ');
	$form->input(array('type'=>'text', 'name'=>'login'));
	$form->label('mdp', 'Password: ');
	$form->input(array('type'=>'password', 'name'=>'mdp'));
	$form->label('base', 'Database Name: ');
	$form->input(array('type'=>'text', 'name'=>'base'));
	$form->label('prefix', 'Tables prefix: ');
	$form->input(array('type'=>'text', 'name'=>'prefix'));
	echo '</div>';
	$form->endfieldset();
	$form->startfieldset();
	$form->legend('Configuration');
	$form->label('lang', 'Language: ');
	scandir_list_file('lang', 'app/lang/', '');
	$form->label('root', 'Home Directory (without the / at the end): ');
	$form->input(array('type'=>'text', 'name'=>'root', 'value'=>str_replace('/install.php','','http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'])));
	echo form_timezone();
	$form->endfieldset();
	$form->startfieldset();
	$form->legend('Administration');
	$form->label('user', 'Login: ');
	$form->input(array('type'=>'text', 'name'=>'user'));
	$form->label('pass', 'Password: ');
	$form->input(array('type'=>'password', 'name'=>'pass'));
	$form->endfieldset();
	$form->input(array('type'=>'submit', 'name'=>'submit'));
	$form->endform();
}
?>
<script type="text/javascript">
	function getSelectSgdb() {
		var selectElmt = document.getElementById("sgbd");
		return selectElmt.options[selectElmt.selectedIndex].value;
	}
	function show_mysql_form() {
		var selected = getSelectSgdb();
		if (selected == "mysql") {
			document.getElementById("mysql_vars").style.display = "block";
			document.getElementById("sqlite_vars").style.display = "none";
		} else {
			document.getElementById("mysql_vars").style.display = "none";
			document.getElementById("sqlite_vars").style.display = "block";
		}
	}


</script>
</body>
</html>