<meta charset="utf-8"/>
<?php
error_reporting(0);
function scandir_list_file($name, $dirname, $selectvar) {
	echo '<select name="'.$name.'">'.PHP_EOL;
	$scandir = scandir($dirname);
	foreach($scandir as $file) {
		if($file != '.' && $file != '..' && !is_dir($dirname.$file)) {
			$arr_file = explode('.',$file);
			$select = '';
			if($arr_file['0'] == $selectvar) {$select='selected'; }
				echo '<option value="'.$arr_file['0'].'" '.$select.'>'.$arr_file['0'].'</option>';
		}
	}
	echo '</select><br>'.PHP_EOL;
}
function translate($array, $termes) {
	if (array_key_exists($array,$termes)) {
		return $termes[$array];
	}
	else {
		return '#lang_'.$array.'#';
		mylog('i18n: "'.$array.'" not found', 'debug');
	}
}
$lang = (!isset($_GET['lang'])) ? 'en' : $_GET['lang'];
echo '<form method="get">';
echo scandir_list_file('lang', 'lang', $lang).'<input type="submit"></form>';
define('CAPTCHA', '".CAPTCHA."');
include("lang/fr.php");
$termes_original = $termes;
include("lang/".$lang.".php");
$termes_trad = $termes;
echo '<form method="post" action="?lang='.$lang.'">';
echo '<table>
	<tr>
		<td>Clé</td>
		<td>Original</td>
		<td>Traduction</td>
	</tr>';
foreach ($termes_original as $key => $value) {
    echo "<tr>";
    echo "<td>".$key."</td>";
    echo "<td>".$value."</td>";
	echo '<td><input type="text" value="'.$termes_trad[$key].'" name="'.$key.'"/></td>';
	echo "</tr>";
}
echo '</table>';
echo '<input type="submit" name="submit"/>';
if(isset($_POST['submit'])) {
	$data = '<?php'.PHP_EOL.'$termes = array();'.PHP_EOL;
	foreach ($termes_trad as $key => $value) {
		$data .= '$termes["'.$key.'"] = "'.$_POST[$key].'";'.PHP_EOL;
	}
	// echo '<pre>'.$data.'</pre>';
	file_put_contents('lang/'.$lang.'.php', $data);
	echo 'Merci de votre soutien pour la traduction !';
	header("Location: index.php");
}
?>