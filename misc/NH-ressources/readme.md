Ressources
=========

Des ressources diverses sur NewsHTML. Plugins, designs, logos, logiciels...
Plugins
=======

[Calendrier](ressources/calendar.phps)
-----------------------------------------------

Theme
-----

^**⚠\\ Non\\ maintenu**^

-   Nom du créateur : qwerty
-   Licence : gpl
-   Modifié le : 30/07/2015
-   Gestion des liens.

[Télécharger](ressources/theme.zip)

Theme
-----

^**⚠\\ Non\\ maintenu**^

-   Nom du créateur : qwerty
-   Licence : by-nc-nd
-   Modifié le : 11/04/2011
-   Permet de modifier les fichiers de designs en ligne.

[Télécharger](ressources/theme.zip)

Contact
-------

-   Nom du créateur : qwerty
-   Licence : by
-   Modifié le : 02/12/2013
-   Ajoute un formulaire de contact

[Télécharger](ressources/contact.zip)

Reader RSS
----------

^**⚠\\ Non\\ maintenu**^

Nom du créateur : qwerty

Licence : by-nc-nd

Modifié le : 11/07/2011

Un lecteur RSS. L'import export de l'opml n'est pas encore implanté.

[Télécharger](ressources/readerrss.zip)

Update
------

-   Nom du créateur : qwerty
-   Licence : tea Licence
-   Modifié le : 20/05/2013
-   Télécharge la dernière version de NH

[Télécharger](ressources/update.zip)

Mods
====

Social Network
--------------

Nom du créateur : qwerty

Licence : wtf

Modifié le : 15/08/2011

Rajoute un lien de partage vers les différents réseaux sociaux.

[Télécharger](ressources/socialnetwork.zip)

Toolbar
-------

-   Nom du créateur : qwerty
-   Licence : by
-   Modifié le : 02/11/2013
-   Rajoute une toolbar de mise en page dans l'administration.

[Télécharger](ressources/toolbar.zip)

Liens
-----

^**⚠\\ Non\\ maintenu**^

-   Nom du créateur : qwerty
-   Licence : by
-   Modifié le : 02/11/2013
-   Ajoute un système de marque page.

[Télécharger](ressources/liens.zip)

Convert BT2NH
-------------

^**⚠\\ Non\\ maintenu**^

-   Nom du créateur : qwerty
-   Licence : by
-   Modifié le : 28/04/2012
-   Convertit les données d'export blogotext en une SQL pour NewsHTML

[Télécharger](ressources/convert_bh2nh.zip)

Langues
=======

Pour avoir le fichier, enregistrer sous, puis renommez .txt en .php.
Mettez dans votre dossier app/lang/.

\
Si vous voyez des erreurs de traductions, contactez moi via le forum....
Juste l'Anglais et le Français seront officiellement entretenus. Les
autres le seront si j'ai le temps et le courage. Ceux en ^Bêta^ sont
traduits grâce à un traducteur en ligne et à mes connaissances et
recherches. les ^Alpha^ sont ceux pas entièrement traduits. Si une
langue manque et que vous voulez bien traduire, veuillez me contacter.

-   [Anglais](lang/en.txt) (en)
-   [Francais](lang/fr.txt) (fr)
-   [Espéranto](lang/eo.txt) (eo)^Bêta^
-   [Italien](lang/it.txt) (it)^Bêta^
-   [Latin](lang/la.txt) (la) ^Alpha^
-   [Allemand](lang/de.txt) (de)^Bêta^ (Merci Timo)
-   [Néerlandais](lang/nl.txt) (nl)^Bêta^ (Merci Timo)
-   [Espagnol](lang/es.txt) (es)^Bêta^ (Merci Last-Geek)

Designs
=======

Vous pouvez m'envoyer un mail si vous voulez voir votre œuvre ici !

Ghost
-----

-   Nom du créateur : Vinm
-   Licence :Beerware.
-   Description : Un design flat et simple.

![](ressources/flat-kiss.png)\
[télécharger](ressources/flat-kiss.zip)

Ghost
-----

-   Nom du créateur : qwerty
-   Licence : Creatives Commons BY-NC.
-   Description : Un design sobre, moderne et élégant.

![](ressources/ghost.png)\
[télécharger](ressources/ghost.zip)

Modern
------

-   Nom du créateur : qwerty et Vinm
-   Licence : Bannière sous licence Créative Commons BY-SA
    (dany.kien.free.fr), Font-icons sous licence SIL par Daniel
    Bruce (fontello.com). Design sous licence conjointe Tea Licence et
    Beer Licence.
-   Description : Un design sobre, moderne et élégant, inspiré d'un
    thème wordpress du même nom

![](ressources/modern.png)\
[télécharger](ressources/modern.zip)

Sky
---

-   Nom du créateur : qwerty et Vinm
-   Licence : Design sous licence conjointe Tea Licence et Beer Licence.
-   Description : Un design sobre, moderne et élégant

![](ressources/sky.png)\
[télécharger](ressources/sky.zip)

Zeitung
-------

^**⚠\\ Non\\ maintenu**^

-   Nom du créateur : qwerty
-   Licence : by-nc
-   Modifié le : 30/06/2013
-   Description : Un design minimaliste

![](ressources/zeitung.png)\
[télécharger](ressources/zeitung.zip)

Voltaire
--------

-   Nom du créateur : qwerty
-   Police : FanWood par [Barry Schwartz](http://www.crudfactory.com)
-   Licence : by-nc
-   Modifié le : 05/08/2013
-   Un design minimaliste et dans le style des vieux livres.

![](ressources/voltaire.png)\
[télécharger](ressources/voltaire.zip)

Pur
---

^**⚠\\ Non\\ maintenu**^

-   Nom du créateur : qwerty
-   Licence : by-nc
-   Modifié le : 30/06/2013
-   Description : Un design minimaliste

![](ressources/pur.png)\
[télécharger](ressources/pur.zip)

GC Blue
-------

^**⚠\\ Non\\ maintenu**^

-   Nom du créateur : fab@c++
-   Licence : by
-   Modifié le : 24/07/2011
-   Un design moderne bleu.

![](ressources/gc-blue.png)\
\
[télécharger](ressources/gc-blue.zip)

GC Marron
---------

^**⚠\\ Non\\ maintenu**^

-   Nom du créateur : fab@c++
-   Licence : by
-   Modifié le : 24/07/2011
-   Un design moderne marron.

![](ressources/gc-marron.png)\
\
[télécharger](ressources/gc-marron.zip)

Facebook
--------

^**⚠\\ Non\\ maintenu**^

-   Nom du créateur : qwerty inspiré du site du même nom.
-   Licence : by-nc
-   Modifié le : 25/01/2012
-   Un design bleu inspiré du site du même nom

![](ressources/facebook.png)\
\
[télécharger](ressources/facebook.zip)

Le geek café
------------

^**⚠\\ Non\\ maintenu**^

-   Nom du créateur : qwerty inspiré d'un design de fab@c++
-   Licence : by-nc
-   Modifié le : 25/01/2012
-   Un design qui reprend la charte graphique de legeekcafe.com mais en
    full CSS.

![](ressources/gctheme.png)\
[télécharger](ressources/gctheme.zip)
