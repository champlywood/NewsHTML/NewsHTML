<?php
$termes = array();
 
$termes['modifier'] = 'Modificar';
$termes['supprimer'] = 'Suprimir';
$termes['titre'] = 'Título';
$termes['date'] = 'Fecha';
$termes['contenu'] = 'Contenido';
$termes['envoyer'] = 'Enviar';
$termes['backliste'] = 'Lista de los artículos';
$termes['upload'] = 'Subir';
$termes['uploaderreur'] = 'Es imposible copiar el archivo en';
$termes['addnews'] = 'Añadir un artículo';
$termes['notimage'] = 'El archivo no es un imagen';
$termes['maj'] = 'Actualización';
$termes['newversion'] = 'Hay una nueva versión';
$termes['download'] = 'Descargarla';
$termes['lastversion'] = 'Usted tiene la última versión';
$termes['filenotfound'] ='Es imposible encontrar el archivo';
$termes['errorcopyupload'] ='Es imposible copiar el archivo';
$termes['uploadok'] ='El archivo ha bien sido subido';
$termes['login'] = 'Identificador';
$termes['password'] = 'Contraseña';
$termes['error'] = 'Error';
$termes['logout'] = 'Desconectarse';
$termes['plugin'] = 'Complementos';
$termes['comment'] = 'Comentarios';
$termes['comment0'] = 'Comentario';
$termes['pseudo'] = 'Seudónimo';
$termes['mail'] = 'Correo electrónico';
$termes['site'] = 'Sitio web';
$termes['message'] = 'Mensaje';
$termes['notifmail'] = 'Notificaciones por correo electrónico';
$termes['noid'] = 'El identificador del artículo no está definido.';
$termes['thinkcomment'] = "Gracias por haber comentado !";
$termes['tag'] = "etiqueta engomada";
$termes['cookie'] = "Recordar estas informaciones en una galleta informática ?";
$termes['config'] = "Configuración";
$termes['sqlconnexion'] = "Conexión SQL";
$termes['hote'] = "Anfitrión";
$termes['database'] = "Base de datos";
$termes['lang'] = "Lengua";
$termes['root'] = "Directorio raíz";
$termes['typeofdb'] = "Tipo de base de datos";
$termes['style'] = "Diseño";
$termes['description'] = "Descripción";
$termes['nb_message_page'] = "Número de artículos por pagina";
$termes['text_resume'] = "Resumen del articulo";
$termes['text_resume_nb'] = "Número de palabras del resumen";
$termes['prefixe'] = "Prefijo";
$termes['brouillon'] = "Borrador";
$termes['name'] = "Nombre";
$termes['extension'] = "Extensión";
$termes['size'] = "Peso del archivo";
$termes['notfound'] = "Ningunos resultados";
$termes['dateformat'] = "DD/MM/AAAA/hh/mm/ss";
$termes['news'] = "artículos";
$termes['validationcomment'] = "Se está validando el comentario";
$termes['validation'] = "validación";
$termes['moderation'] = "Validación de los comentarios a priori";
$termes['format_date'] = "Formato de la fecha";
$termes['debug'] = "Habilitar la depuración";
$termes['tag_separator'] = "separador de la etiqueta";
$termes['captcha'] = "CAPTCHA";
$termes['typecaptcha'] = "escriba la palabra ".CAPTCHA;
$termes['author'] = "Author";
$termes['admin'] = "Administrator";
$termes['statut'] = "statut";
$termes['addauthor'] = "Add an author";
$termes['readingtime'] = "Reading time:";
$termes['format_url'] = "URL format:";
$termes['format_parse'] = "Syntax:";
$termes['enable_cache'] = "Enable the cache:";
?>