<?php
	$date_m = (isset($_GET['date_m'])) ?  $_GET['date_m'] : date('n');
	$date_y = (isset($_GET['date_y'])) ?  $_GET['date_y'] : date('Y');
	$mois_nom = array (
		1 => 'janvier',
		2 => 'février',
		3 => 'mars',
		4 => 'avril',
		5 => 'mai',
		6 => 'juin',
		7 => 'juillet',
		8 => 'aout',
		9 => 'septembre',
		10 => 'octobre',
		11 => 'novembre',
		12 => 'décembre'
	);
    $jours_semaine = array('L', 'Ma', 'Mer', 'J', 'V', 'S', 'D');

    $premier_jour = mktime('0', '0', '0', $date_m, '1',$date_y);
	if($date_m + 1 > 12) {
		$date_m_before = $date_m - 1;
		$date_y_before = $date_y;
		$date_m_after =  1;
		$date_y_after = $date_y +1;
	}
	elseif($date_m -1 <=  1) {
		$date_m_before = 12;
		$date_y_before = $date_y -1;
		$date_m_after =  $date_m+1;
		$date_y_after = $date_y;
	}
	else {
		$date_m_before = $date_m - 1;
		$date_y_before = $date_y;
		$date_m_after = $date_m + 1;
		$date_y_after = $date_y;
	}
    // haut du tableau
	$begin_time = mktime(0, 0, 0, $date_m, 1, $date_y);
	$last_time = mktime(23, 59, 59, $date_m, date('t', $begin_time), $date_y);
	$something = $bdd->query('SELECT id, timestamp, title FROM news WHERE timestamp >= '.$begin_time.' AND timestamp <='.$last_time);
	$article = array();
	while($data = $something->fetch()) {
		$article[date('j/Y',$data['timestamp'])]['id'] = $data['id'];
		$article[date('j/Y',$data['timestamp'])]['title'] = $data['title'];
	}
    echo '<table id="calendrier">'."\n";
    echo '<caption><a href="admin.php?plugin=calendar.php&action=plugin&date_m='.$date_m_before.'&date_y='.$date_y_before.'">«</a> '.$mois_nom[$date_m].' '.$date_y.' <a href="admin.php?plugin=calendar.php&action=plugin&date_m='.$date_m_after.'&date_y='.$date_y_after.'">»</a></caption>'."\n";
    echo'<tr><th>'.implode('</th><th>', $jours_semaine).'</th></tr><tr>';

    // un nouveau mois ne commence pas forcément le premier jour de la semaine (lundi)
    $decalage_jour = date('w', $premier_jour-'1');
    if ($decalage_jour > 0) {
        for ($i = 0; $i < $decalage_jour; $i++) {
           echo  '<td></td>';
        }
    }

    // création du tableau
    $date_t = date('t');
    $date_d = date('d');
		for ($jour = 1; $jour <= $date_t; $jour++) {
			if($jour == $date_d && $date_y == date('Y') && $date_m ==date('n') && array_key_exists($jour.'/'.$date_y, $article)) {
				echo '<td><b><i><a href="'.url_format($article[$jour.'/'.$date_y]['id']).'" title="'.$article[$jour.'/'.$date_y]['title'].'">'.$jour.'</a></i></b></td>';
			}
			elseif($jour == $date_d && $date_y == date('Y') && $date_m ==date('n')) {
				echo '<td><b>'.$jour.'</b></td>';
			}
			elseif(array_key_exists($jour.'/'.$date_y, $article)) {
				echo '<td><a href="'.url_format($article[$jour.'/'.$date_y]['id']).'" title="'.$article[$jour.'/'.$date_y]['title'].'">'.$jour.'</a></td>';
			}
			else {
				echo '<td>'.$jour.'</td>';
			}
			$decalage_jour++;
			if ($decalage_jour == '7') {
				$decalage_jour = '0';
			   echo  '</tr>'."\n";
				echo ($jour < $date_d) ? '<tr>' : '';
			}
		}
	
    // complète le tableau HTML avec des cases vides à la fin
    if ($decalage_jour > '0') {
        for ($i = $decalage_jour; $i < '7'; $i++) {
            echo '<td></td>';
        }
        echo '</tr>'."\n";
    }
    echo '</table>'."\n";

