<?php
function dl($file, $from, $to) {
	file_put_contents($file, file_get_contents($from));
	$zip = new ZipArchive;
	if ($zip->open($file) === TRUE) {
		if(!is_dir($to)) { mkdir($to);}
		$zip->extractTo('newshtml/'.$to);
		$zip->close();
		@unlink($file);
	}
}
function dllang($lang) {
	file_put_contents('app/lang/'.$lang.'.php', file_get_contents('http://champlywood.free.fr/newshtml/archives/lang/'.$lang.'.txt'));
}
function Zip($source, $destination) {
	if(!extension_loaded('zip') || !file_exists($source)) {
		return false;
	}

	$zip = new ZipArchive();
	if(!$zip->open($destination, ZIPARCHIVE::CREATE)) {
		return false;
	}
	$source = str_replace('\\', '/', realpath($source));
	if(is_dir($source) === true) {
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

		foreach($files as $file) {
			$file = str_replace('\\', '/', $file);
			// Ignore "." and ".." folders
			if(in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
			continue;
			$file = realpath($file);
			if(is_dir($file) === true) {
				$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
			}
			else if (is_file($file) === true) {
				$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
			}
		}
	}
	else if (is_file($source) === true) {
		$zip->addFromString(basename($source), file_get_contents($source));
	}
	return $zip->close();
}
function erased_dir($dir) {
	if(!is_dir($dir)) {
		throw new InvalidArgumentException("$dir must be a directory");
	}
	if(substr($dir,strlen($dir)-1,1) != '/') {
		$dir .= '/';
	}
	$files = glob($dir.'*',GLOB_MARK);
	if($files) {
		foreach ($files as $file) {
			if(is_dir($file)) {
				erased_dir($file);
			} 
			else {
				unlink($file);
			}
		}
	}
	rmdir($dir);
}
dl('newshtml.zip', 'http://champlywood.free.fr/newshtml/newshtml.zip', './');
dl('theme.zip', 'http://champlywood.free.fr/newshtml/archives/ressources/theme.zip', 'app/plugin');
dl('flat-kiss.zip', 'http://champlywood.free.fr/newshtml/archives/ressources/flat-kiss.zip', 'app/design');
dl('ghost.zip', 'http://champlywood.free.fr/newshtml/archives/ressources/ghost.zip', 'app/design/ghost');
dl('modern.zip', 'http://champlywood.free.fr/newshtml/archives/ressources/modern.zip', 'app/design/modern');
dl('gc-blue.zip', 'http://champlywood.free.fr/newshtml/archives/ressources/gc-blue.zip', 'app/design/gc-blue');
dl('gc-marron.zip', 'http://champlywood.free.fr/newshtml/archives/ressources/gc-marron.zip', 'app/design/gc-marron');
dllang('eo');
dllang('it');
dllang('la');
dllang('de');
dllang('nl');
dllang('es');
Zip('newshtml/', 'newshtml.zip');
file_put_contents('lastversion.txt', file_get_contents('http://champlywood.free.fr/newshtml/lastversion.txt'));
erased_dir('newshtml/');