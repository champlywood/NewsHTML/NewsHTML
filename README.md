NewsHTML est un gestionnaire de news utilisant MySQL ou SQLite. Léger, rapide, simple, modulaire, il offre d'exellentes performances. Il respecte le principe du KISS (Keep it simple, Stupid, ne complique pas les choses).

Mis en ligne le mardi 28 décembre 2010.

# Technologies

NewsHTML utilise PHP 5.3, SQLite et MySQL. SQLite est un gestionnaire de base de données simple à mettre en place (stocké dans un fichier) et qui offre de bonnes performances. Il est autant utilisé dans des systèmes embarqués (GPS, Android, iOS), que dans des applications telles que Firefox ou Opera. MySQL est aussi un gestionnaire de base de données. Plus long à mettre en place (il necessite de rentrer des identifiants), il est adapté pour les gros volumes de données (plusieurs milliers d'articles) et possède de bonnes performances. Il est plus répandu parmi les hébergeurs web que le SQLite (même si ce dernier n'est qu'un module qui peut être facilement activé chez votre prestataire). A noter que NewsHTML est compatible avec MariaDB. Pour l'utiliser, choisir l'option "MySQL".

# Personalisation

NewsHTML est un logiciel personnalisable et modulaire. Vous pouvez d'ailleurs télécharger des thèmes sur le site. Mais NewsHTML est bien plus qu'un outil de blog, et vous pouvez rajouter des fonctionnalités, comme par exemple un lecteur de flux RSS, un partage/sauvegarde de liens (shaarli-like), de faire du microblogging… NewsHTML est aussi disponible dans plusieurs langues dont le français, l'anglais, l'allemand, l'italien,…

Documentation
=============

-   [Installation](#install)
    -   [Si c'est la première fois](#firstinstall)
    -   [Si c'est une mise à jour](#maj)
    -   [Mise à jour 3 vers supérieur](#maj3sup)
-   [Utilisation et personnalisation](#utilisation)
    -   [créer un article](#creerarticle)
    -   [Modifier et supprimer un article](#modifierarticle)
    -   [Installer un plugin](#installplugin)
    -   [Utiliser un plugin](#useplugin)
    -   [Activer/désactiver les commentaires](#commentaires)
    -   [Mettre la validation des commentaires à priori](#moderation)
    -   [Personnalisation du design](#design)
    -   [Faire un résumé de l'article](#resumearticle)
    -   [La langue](#lang)
    -   [la mise en forme](#miseenforme)
    -   [Comment suivre un flux RSS selon un tag donné ?](#rsstag)
-   [Developpement](#dev)
    -   [Plugins](#devplugins)
    -   [Mods](#devmods)
    -   [Design](#devdesign)
    -   [Requêtes SQL](#devsql)
    -   [Snippets PHP](#devphp)
    -   [Snippets JS](#devjs)
-   [Bugs](#bugs)
    -   [PHP5](#bugphp5)
    -   [Sessions chez free.fr](#bugsessionfree)
    -   [Bugs d'encodage dans les commentaires](#encodage)
    -   [Free.fr et la PDO](#freepdo)

Installation {#install}
------------

### Si c'est la première fois {#firstinstall}

Envoyez tout sur votre serveur et allez sur la page "install.php"

### Si c'est une mise à jour {#maj}

Téléversez tout sur votre serveur sauf
`             install.php             maj.php/             data/             `{style="display:block"}

### Mise à jour 3 vers supérieur {#maj3sup}

De nombreux changements ont été fait sur NewsHTML, notamment sur la
structure et la création des comptes multi-auteurs.\
Comment alors migrer de la version 3 à la version supérieur ?\
Tout d’abord faites une sauvegarde de votre site.\
Ensuite, mettez dans un dossier à par design/VOTRE DESIGN, data.sqlite,
upload.txt, includes/config.php et le dossier img/.\
Ouvrez à l’aide de votre éditeur de texte préféré (de type notepad++) le
fichier config.php\
Téléchargez le module pour firefox « SQLITE MANAGER ». Ouvrez
data.sqlite avec\
Dans config.php, cherchez la valeur de PASSWORD\_USER.\
Dans « executer le sql » de Sqlite Manager, copier coller le code
ci-dessous, en veillant à remplacer PASSWORD\_USER par le code que vous
avez copiez-collez, et LOGIN par la valeur de LOGIN\_USER.

    CREATE TABLE author (id_author INTEGER PRIMARY KEY,pseudo text NOT NULL,name text NOT NULL,password text NOT NULL,privilege int(20) NOT NULL);
    INSERT INTO author ( pseudo, name, password, privilege) VALUES ('LOGIN','LOGIN', 'PASSWORD_USER', 1 );
    ALTER TABLE news ADD COLUMN author_id;
    UPDATE news SET author_id='1';
    UPDATE news SET contenu = REPLACE(contenu, '/img/', '/data/upload/');

renommez img/ par upload/.\
Créez un dossier data/. Mettez dedans upload/, data.sqlite, upload.txt,
config.php.\
Ouvrez maintenant design/VOTRE DESIGN/tpl/comment.php et design/VOTRE
DESIGN/tpl/comment.php. Remplacez {variable} par &lt;?php echo
\$variable;?&gt;.\
Dans article.php, supprimez \[comment\]

Installez maintenant une nouvelle version de NewsHTML sur votre serveur.
Mettez votre dossier design/ dans app/. Remplacer le dossier data/ par
le vôtre. Allez dans votre administration dans l’onglet
« configuration ». Mettez à jour en modifiant au besoin des données (ou
pas) et cliquez sur envoyer (cela sert à mettre en place de nouvelles
entrées dans la configuration).

Utilisation
-----------

### créer un article {#creerarticle}

Allez dans l'Administration.\
Cliquer sur "Ajouter une news". Vous tombez sur une page avec deux
champs de texte. Le premier sert à marquer le titre, le second le
contenu du billet. La mise en forme se fait en html ou le langage de
balisage léger propre à NewsHTML (voir la section "mise en forme").

Pour uploader une image, cliquer sur "Téléversement", puis sur
"Parcourir", puis choisissez votre image. Cliquez sur "téléverser". Un
code s'affiche. Copiez collez le dans votre zone de rédaction.

### Modifier et supprimer un article {#modifierarticle}

Retournez a l'accueil de l'administration. Pour modifier votre billet,
cliquez sur l'icône représentant un crayon, pour supprimer, cliquer sur
la croix rouge. Attention, la suppression est irréversible !

### Installer un plugin {#installplugin}

Il faut envoyer le fichier dans le répertoire app/plugin/.

### Utiliser un plugin {#useplugin}

Allez dans la section "plugin", et cliquer sur le plugin de votre choix.

### Activer/désactiver les commentaires {#commentaires}

Vous pouvez passer par l'administration, dans l'onglet configuration.\
Si vous voulez le faire manuellement, vous pouvez aller dans
data/config.php.Pour les activer, remplacez à la ligne
`$comment = false;`{style="display:inline;"} par
`$comment = true;`{style="display:inline;"}. Pour les désactivés, faites
l'action contraire.

### Mettre la validation des commentaires à priori {#moderation}

Vous pouvez passer par l'administration, dans l'onglet configuration.\
Si vous voulez le faire manuellement, vous pouvez aller dans
data/config.php.Pour les activer, remplacez à la ligne
`MODERATION = false;`{style="display:inline;"} par
`MODERATION = true;`{style="display:inline;"}. Pour les désactivés,
faites l'action contraire.

Dans la base de donnée, sur la colonne moderation, 0 signifie que le
commentaire est en cours de validation et 1 qu'il est validé.

### Personnalisation du design {#design}

Si vous avez des connaissances en HTML/CSS, vous pouvez modifier les
fichiers dans le dossier app/VOTREDESIGN/ pour mieux personnaliser votre
site. Vous pouvez aussi utiliser les designs dans la rubrique "Themes"
du site officiel. Si vous voulez en créer un, il doit être de la
structure suivante :\

-   app/design/LE\_NOM\_DE\_VOTRE\_DESIGN/style.css
-   Un dossier tpl dans app/design/LE\_NOM\_DE\_VOTRE\_DESIGN/tpl

.

### La langue {#lang}

Vous pouvez passer par l'administration, dans l'onglet configuration.
Sinon, vous pouvez aller dans le fichier config.php. Remplacer fr par en
pour mettre en anglais ou inversement. En bref, mettez le code entre
parenthèse à côté du téléchargement du fichier langue.

### la mise en forme {#miseenforme}

La mise en page se fait en HTML. Voici un petit mémo pour vous aider :\
&lt;p&gt;&lt;/p&gt; : Pour un paragraphe.\
&lt;hr /&gt; : Tracer une ligne horizontal.\
&lt;br /&gt;: Retour à la ligne.\
&lt;i&gt;&lt;/i&gt; : Texte en italique.\
&lt;b&gt;&lt;/b&gt; : texte en gras.\
&lt;s&gt;&lt;/s&gt; : texte barré.\
&lt;u&gt;&lt;/u&gt; : Souligne un texte.\
&lt;strong&gt;&lt;/strong&gt; : Met un texte en important.\
&lt;em&gt;&lt;/em&gt; : met le texte en moins important.\
&lt;sup&gt; &lt;/sup&gt; : Mise en exposant.\
&lt;sub&gt; &lt;/sub&gt; : Mise en indice.\
&lt;em&gt; &lt;/em&gt; : emphase.\
&lt;strong&gt; &lt;/strong&gt; : emphase forte.\
&lt;time&gt; &lt;/time&gt; : une date.\
&lt;abbr title="abréviation complète"&gt; abréviation &lt;/abbr&gt; :
votre abréviation.\
\
&lt;ul&gt;\
&lt;li&gt;1er élément de la liste.&lt;/li&gt;\
&lt;li&gt;2ème élément de la liste.&lt;/li&gt;\
&lt;li&gt;3ème élément de la liste.&lt;/li&gt;\
&lt;/ul&gt;\
&lt;h1&gt;&lt;/h1&gt; : titre de niveau 1\
&lt;h2&gt;&lt;/h2&gt; : sous-titre de niveau 2\
&lt;h3&gt;&lt;/h3&gt; : sous-titre de niveau 3\
&lt;h4&gt;&lt;/h4&gt; : sous-titre de niveau 4\
&lt;h5&gt;&lt;/h5&gt; : sous-titre de niveau 5\
&lt;h6&gt;&lt;/h6&gt; : sous-titre de niveau 6\
\
&lt;del&gt;un texte supprimé&lt;/del&gt;\
&lt;ins&gt;un texte rajouté&lt;/ins&gt;\
&lt;mark&gt;un texte surligné&lt;/mark&gt;\
&lt;kbd&gt;une touche de clavier&lt;/kbd&gt;\
\
&lt;blockquote&gt;un bloc de citation&lt;/blockquote&gt;\
&lt;q&gt;une courte citation&lt;/q&gt;\
&lt;cite&gt;L'auteur d'une citation&lt;/cite&gt;\
\
&lt;figure&gt;[Unité de
contenu](http://www.alsacreations.com/article/lire/1337-html5-elements-figure-et-figcaption.html)
(image, vidéo...)&lt;/figure&gt;\
&lt;figcaption&gt;légende d'une figure&lt;/figcaption&gt;\
\
&lt;code&gt;code source informatique&lt;/code&gt;\
&lt;samp&gt;résultat d'un code source&lt;/samp&gt;\
&lt;var&gt;une \$variable d'un code source&lt;/var&gt;\
\
&lt;video&gt;une vidéo&lt;/video&gt;\
&lt;audio&gt;bande son&lt;/audio&gt;\
\
&lt;dl&gt;\
&lt;dt&gt;Nom&lt;/dt&gt;\
&lt;dd&gt;Définition&lt;/dd&gt;\
&lt;/dl&gt;\
\
&lt;img src="adressedel'image" alt="texte alternative au cas où elle ne
s'affiche pas !" /&gt; : insert une image.\
&lt;a href="http://adresseweb.com"&gt;adresse web, ou texte&lt;/a&gt; :
pour un lien hypertexte.

------------------------------------------------------------------------

La mise en forme peut aussi se faire à l'aide d'une syntaxe à [balisage
léger](https://fr.wikipedia.org/wiki/Langage_de_balisage_l%C3%A9ger)
inspirée du Markdown :\
\_mettre en italique\_\
\*Mettre en gras\*\
\~\~Texte supprimé\~\~\
\[\[Texte surligné\]\]\
\#1 titre de niveau 1 \#\
\#2 titre de niveau 2 \#\
\#3 titre de niveau 3 \#\
\#4 titre de niveau 4 \#\
\#5 titre de niveau 5 \#\
\#6 titre de niveau 6 \#\
\#\# liste numérotée\
-- liste non numérotée\
Insérer un lien : \[http://example.org|Votre Lien\] ou \[Votre
lien\](http://example.org) (markdown-like)\
Inserer une image : {http://example.org/image.png|libellé} ou
!\[libellé\](http://example.org/image.) (markdown-like)\
mettre en exposant : \^(exposant)\
mettre en indice : \_(indice)\
Faire une ligne horizontale : ----- (minimum 5 tirets)\
Note de pied de page : \\footnote{note bene lorem ipsum}\
Si vous voulez combiner des mises en formes, pensez à mettre un espace
entre (par exemple \_ \*test\* \_).

### Comment suivre un flux RSS selon un tag donné ? {#rsstag}

Il suffit d'aller sur rss.php?tag=votretag

Developpement {#dev}
-------------

### Plugins {#devplugins}

Les plugins ne sont pas vraiment des mods dans la mesure où il ne
nécessite pas de modifier de fichiers. Il suffit de le téléverser dans
admin/plugin. Vous pouvez le coder vous-même, si vous connaissez le PHP.
Le fichier doit être encodé en UTF-8 sans BOM et être codé en Procédural
(je n'ai pas testé en POO).

Voici le code de base :

    <?php
    /* ***********************************************************
    Mod title:  [Le nom du mod]
    Mod version:  [le n°  de version]
    Release date:  [La date de mise à jour sous cette forme d/m/y]                                  
    Author:  [Pseudo et mail/site, ou tout autre moyen de contact]
    Description:  [Une description du mod]
    Affected files:  [les fichiers modifié]     
    Affects DB:  [si ça affecte la base de donnée]
    Notes:  [Si vous avez quelque chose de particulier à dire]
    DISCLAIMER:   L'installation de ces modifications
    est à vos risques et périls. Faites un backup de votre base
    de données et sauvegardez les fichiers concernés par la mod.
    ************************************************************** */
    ?>
    <h1>Titre du mod</h1>
    <?php
    //Your code
    ?>

### Mods {#devmods}

Un mod est une modification du code déjà existant. Il est composé d'un
fichier readme.txt pour modifier les fichiers déjà existants et de
nouveaux fichiers si nécessaire. Il reprend l'architecture des dossiers.

Voici à quoi ressemble un readme.txt.\
Étudions l'entête :\

    ************************************************************
    Mod title:  [Le nom du mod]
    Mod version:  [le n°  de version]
    Release date:  [La date de mise à jour sous cette forme d/m/y]                                  
    Author:  [Pseudo et mail/site, ou tout autre moyen de contact]
    Description:  [Une description du mod]
    Affected files:  [les fichiers modifié]     
    Affects DB:  [si ça affecte la base de donnée]
    Notes:  [Si vous avez quelque chose de particulier à dire]
    DISCLAIMER:   L'installation de ces modifications
    est à vos risques et périls. Faites un backup de votre base
    de données et sauvegardez les fichiers concernés par la mod.
    **************************************************************

\
\
Après des instructions sont données :\

    -[IMPORTER DANS VOTRE BASE DE DONNEE ]-
     comment.sql

    -[OUVRER]-
    index.php

    -[TROUVER]-
    $contenu = nl2br(stripslashes($donnees['contenu']));

    -[AJOUTER APRES]-
    $commentaire =  '<a href="commentaires.php?idnews='.$id.'&titrenews='.$titre.'">commentaires</a>';

    -[TROUVER]-
    <p> <?php echo $contenu; ?> </p>
    -[AJOUTER APRES]-
    <p><?php echo $commentaire; ?></p>

### Design {#devdesign}

Si vous avez des connaissances en HTML/CSS, vous pouvez modifier les
thèmes (app/design), pour mieux personnaliser votre site. Vous pouvez
ajouter les fichiers toolbar.php (rajouter une barre d'outil),
comment\_msg.php (afficher un message pour les commentaires).\
Les templates sont les templates par défaut de PHP, sous la forme &lt;?=
\$truc; ?&gt;. Voici à quoi correspondent les variables :

-   article.php
    -   id : l'identifiant unique de l'article.
    -   titre : le titre.
    -   date : la date affichée.
    -   datetime : la date en microformat.
    -   contenu : le contenu.
    -   tags : vos tags.
-   comment.php
    -   id : l'identifiant unique de l'article.
    -   date : la date affichée.
    -   datetime : la date en microformat.
    -   pseudo : le pseudo.
    -   message : le message.
    -   gravatar : l'avatar.
-   comment\_msg.php : un messaga avant le formulaire de contact.
-   pagination.php : afficher la pagination.
-   header.php : l'entête du site.
-   footer.php : le pied de page du site.

Voici un bon CSS de base :\

    .comment {
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    padding: 5px;
    color: black;
    background-color:#EEE;
    border: 1px dashed #ccc;
    }
    .comment .info  img{
    float:left;
    }
    .comment .info  time{
    display:block;
    font-size: 65%;
    }
    .comment blockquote {
    padding: 0px;
    margin-left:59px;
    word-wrap: break-word;
    }
    #tabs li {
    text-decoration: none;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    padding: 5px;
    text-align: center;
    color: white;
    background-color: rgb(230,230,230);
    display:inline-block;
    }
    #tabs li:hover {
    background-color:#7FBAED;
    }
    label {
    display:block;
    float:left;
    width:200px;
    }
    #speedbar{
        margin: 0px 8px 8px 8px;
        height : 32px;
        border-width: 2px 0 0 0;
    }

    #speedbar .recherche{
        float: right;
        width: 220px;
        position: relative;
        top: 3px;
    }

    #speedbar ul{
        display: inline-block;
        margin: 0 0 0 -40px;
        position: relative;
        top: 4px;
    }

    #speedbar ul li{
        width: 120px;
        height: 32px;
        display: inline-block;
        text-align: center;
        font-size: 1.3em;
    }

    #speedbar ul li a{
        text-decoration: none;
    }
    h1 a {
    color:#000;
    text-decoration:none;
    }

\
Vous pouvez aussi rajouter, pour que vous boutons soit plus jolies :\

    #tabs li {
    text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
    border: 1px solid rgb(230,230,230);
    border-bottom-color: #BBB;
    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
    -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
    box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
    text-decoration : none;
    }

### Requêtes SQL {#devsql}

Vous pourrez avoir besoin de faire des requêtes SQL pour obtenir un
résultat. En voici quelques unes.

-   Pour migrer d'une adresse à une autre les images :
    `UPDATE prefixe_news SET contenu= REPLACE(contenu,"http://ancienne.adresse","http://nouvelle.adresse")`
-   Pour connaitre le top posteur :
    `SELECT mail, COUNT( * ) FROM prefixe_commentaires GROUP BY mail ORDER BY COUNT( * ) DESC LIMIT 10 `

### Snippets en PHP {#devphp}

#### La liste de tout les tags (nuage)

``

    debug("top");
        $retour = $bdd->query('SELECT tag FROM '.PREFIXE.'news WHERE timestamp < '.time().' AND brouillon="0" ORDER BY timestamp DESC');
        debug("query");
        $list_tag ="";
        while($donnees = $retour->fetch()) {
            $list_tag .= $donnees['tag'].' ';
        }
        $tab_tags = explode(' ', $list_tag);
        $tab_tags = array_unique($tab_tags);
        sort($tab_tags);
        if ($tab_tags[0] == '') {
        array_shift($tab_tags);
        }
        $tag = $tab_tags;
        $nombre_tag = count($tag);
        $tags = '';
        for ($numero = 0; $numero < $nombre_tag; $numero++) {
            $retour = $bdd->query('SELECT count(id) FROM '.PREFIXE.'news where tag like "%'.$tag[$numero].'%"');
            debug("query");
            $donnees = $retour->fetch();
            $tags .= <a href="search.php?tag='.$tag[$numero].'" style="font-size:'.$donnees['count(id)'].'px;">#'.$tag[$numero].'</a>('.$donnees['count(id)'].')  ';
            
        }

        echo $tags;

Il a été enlevé la fonction de parsage NHtags, car peu usité. Vous
pouvez retrouver la fonction, si vous souhaitez continuer à l’utiliser
(comptabilité), le [code est
disponible](http://champlywood.free.fr/logiciels/newshtml/archives/nhtags.txt)

### Snippets en JS {#devjs}

#### Répondre à

``

    function reply(code) {
        var field = document.getElementsByTagName('textarea')[0];
        field.focus();
        if (field.value !== '') {
            field.value += '\n\n';
        }
        field.value += code;
        field.scrollTop = 10000;
        field.focus();
    }

`<a href="javascript:void(0)" onclick="reply('@[#<?php echo $id;?>|<?php echo strip_tags($pseudo);?>] : ');">Reply</a>`

Bugs
----

### PHP5 {#bugphp5}

Si vous avez des bugs sur la page de mise à jour dans votre
administration ou d'autres fonctionnalités, c'est que votre serveur n'a
pas PHP5. Pour ceux qui ne peuvent pas modifier les configurations du
serveurs, il faut modifier le fichier .htaccess.

-   Chez free.fr :

        php 1

-   Chez OVH :

        SetEnv PHP_VER 5

-   Chez 1&1 :

        AddType x-mapp-php5 .php

-   Chez Online.net :

        AddType application/x-httpd-php5 .php

### Sessions chez free.fr {#bugsessionfree}

Si vous avez des bugs pour vous connecter, notamment chez free.fr, créer
un dossier 'sessions' à la racine du serveur.

### Bugs d'encodage dans les commentaires {#encodage}

dans index.php, enlevez utf8\_encode() et utf8\_decode().

### Free.fr et la PDO {#freepdo}

Chez Free.fr, la PDO et MySQL ne marche pas. En effet, juste la PDO avec
SQLite est activée. Il faudra donc utiliser SQLite.

2010 - 2018
