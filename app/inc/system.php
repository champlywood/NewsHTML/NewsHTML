<?php
//Numero de version :
define('VERSION', 4);
define('NH_ROOT', 'http://champlywood.free.fr/newshtml/');
?>
<?php
if(file_exists('data/config.php')) {
	include 'data/config.php';
	try {
		if(SGBD =='sqlite') {
			$bdd = new PDO('sqlite:data/data.sqlite');
		}
		if(SGBD =='mysql') {
			$bdd = new PDO('mysql:host='.URL_DB .';dbname='.TABLE, LOGIN, PASSWORD);
			$bdd->query('SET NAMES "utf8"');
		}
		$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} 
	catch (PDOException $e) {
		echo '<p>Error : $e->getMessage() </p>';
		mylog('Error :.$e->getMessage()');
		exit();
	}
}
else {
	install();
}
include 'form.class.php';
include 'atom.class.php';
include 'lib/pubsubhubbubpublisher.php';
use pubsubhubbub\publisher\Publisher;
include_once 'lib/Parsedown.php';
include_once 'lib/ParsedownExtra.php';
date_default_timezone_set(TIMEZONE);
setlocale(LC_ALL, SET_LOCALE);
include 'app/lang/'.LANG.'.php';
?>
<?php
## Cache system
if(!is_dir('app/cache/tmp')) { mkdir('app/cache/tmp',0755);}
if(!is_dir('app/cache/tmp/index')) { mkdir('app/cache/tmp/index',0755);}
if(!is_dir('app/cache/tmp/article')) { mkdir('app/cache/tmp/article',0755);}
if(!is_dir('app/cache/tmp/feed')) { mkdir('app/cache/tmp/feed',0755);}
if(!is_dir('app/cache/tmp/admin')) { mkdir('app/cache/tmp/admin',0755);}
function debug($statut) {
	global $querycount;
	global $begin;
	if(DEBUG == true) {
		if($statut == 'top') {
			$begin = microtime(TRUE);
			$querycount = 0;
		}
		if($statut == 'query') {
			$querycount +=1;
		}
		if($statut == 'bottom') {
			$end = microtime(TRUE);
			$return = round(($end - $begin),6).'seconds| '.$querycount.' queries | Memory:'.octectsconvert(memory_get_usage()).' | Peak of memory:'.octectsconvert(memory_get_peak_usage()).' PHP version:'.phpversion();
			mylog($return, 'debug');
			echo $return;
		}
	}
}
function check_extension($n) {
		if(extension_loaded($n)) {echo '<p>Check: '.$n.' is actived :)</p>';} else { echo '<p>uncheck: '.$n.' is disabled :(</p>';}
}
if(DEBUG == true) {
	error_reporting(E_ALL);
	ini_set('display_errors','On');
} 
else { 
	error_reporting(E_ALL);
	ini_set('display_errors','Off');
	ini_set('log_errors', 'On');
	ini_set('error_log', 'app/cache/log/error.log');
}
function tpl_include($file) {
	if(file_exists($file)) {include $file;}
}
function private_form($id) {
	ob_start();
	$form = New form(array('method'=>'post'));
	$form->label('password', translate('password'));
	$form->input(array('type'=>'password', 'name'=>'password'));
	$form->input(array('type'=>'hidden', 'name'=>'id_news', 'value'=>$id));
	$form->input(array('type'=>'submit'));	
	$form->endform();
	$page = ob_get_contents();
	ob_end_clean();
	return $page;
}
function url_format($id, $pattern) {
	return ROOT.sprintf($pattern, $id);
}
function translate($array) {
	global $termes;
	if (array_key_exists($array,$termes)) {
		return $termes[$array];
	}
	else {
		return '#lang_'.$array.'#';
		mylog('i18n: "'.$array.'" not found', 'debug');
	}
}
function stripAccents($string) {
	$string = html_entity_decode($string);
	$string = strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ !;,:','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY-----');
	return $string;
}
function parse($text, $id) {
	switch(FORMAT_PARSE) {
		case 'markdown':
			$md = new ParsedownExtra();
			return $md->text($text); 
		break;
		case 'html':
			return $text;
		break;
		default:
			$md = new ParsedownExtra();
			return $md->text($text); 
		break;
	}
}
function checkupdate() {
		$newversion = file_get_contents(NH_ROOT.'/lastversion.txt'); 
		if ($newversion > VERSION) {
			return true;
		} 

		else { 
			return false;
		} 
}
function readingtime($text) {
		$readingtime =null;
	    $word = str_word_count(strip_tags($text));
        $seconds_estimate = floor($word / (200 / 60));
		$hours = intval($seconds_estimate / 3600);
		$minutes=intval(($seconds_estimate % 3600) / 60);
		$secondes=intval((($seconds_estimate % 3600) % 60));
		if($secondes == 60) {$minutes = 1; $secondes = 0;}
		if($minutes == 60) {$hours = 1; $minutes = 0;}
		if($hours>=1) { $readingtime .= $hours.'h ';}
		if($minutes>=1) { $readingtime .= (ceil($minutes/5)*5).'min ';}
		if($minutes<1 && $hours == 0) { $readingtime .= (ceil($secondes/5)*5).'s ';}
		return $readingtime;
}
function pagination($currentPage, $maxPage, $root='?page=%d') { #src : http://fr.openclassrooms.com/forum/sujet/pagination-et-troncature?page=1#message-85900598
	$voisin_extrem = 2;
	$voisin_middle = 1;
	$prev = $currentPage - 1;
	$next = $currentPage + 1;
	$text = '';
	if($currentPage > 1) {$text .='<a href="'.sprintf($root, $prev).'">◄</a>';}
	for($i = 1; $i <= $maxPage; ++$i) {
		if($i > 1+$voisin_extrem && $i < ($currentPage - $voisin_middle)) {
			$text .= '&hellip; ';
			$i = $currentPage - $voisin_middle;
		} 
		elseif($i > ($currentPage+$voisin_middle) && $i < ($maxPage - $voisin_extrem)) {
			$text .= '&hellip; ';
			$i = $maxPage - $voisin_extrem;
		}
		$text .= ($i != $currentPage ? '<a href="'.sprintf($root, $i).'">'.$i.'</a> ' : $i .' ');
	}
	if($currentPage < $maxPage) {$text .='<a href="'.sprintf($root, $next).'">►</a>';}
	return $text;
}
function inputurl($value) {
	return '<input name="requete" onclick="this.select()" value="'.$value.'" type="text" />';
}
function gravatar($email, $size=60) {
	$default = urlencode('http://www.gravatar.com/avatar.php');
	$email = md5($email);
	$url = 'http://www.gravatar.com/avatar.php';
	$url.= '?gravatar_id=%s';
	$url.= '&amp;size=%d';
	$url.= '&amp;default=%s';
	$url = sprintf($url,$email,intval($size),$default);
	$out = '<img src="'. $url .'" alt="Gravatar" title="Gravatar" />';
	return $out;
}

function install() {
	if(!file_exists('config/config.php') && strpos($_SERVER['SCRIPT_NAME'], 'install.php') == false) {redirect('install.php');}
}
function tab_admin($tab, $word) {
	if($tab == ''){
		if(isset($_GET['action']) && $_GET['action'] != ''){
			return '<a href="admin.php">'.$word.'</a>';
		}
		else{
			return $word;
		}
	}
	else{
		if(isset($_GET['action']) && $_GET['action'] == $tab){
			return $word;
		}
		else {
			return '<a href="?action='.$tab.'">'.$word.'</a>';
		}
	}
}
function octectsconvert($bytes) {
	$unit = array('B','KB','MB');
	return @round($bytes / pow(1024, ($i = floor(log($bytes, 1024)))), 1).' '.$unit[$i];
}
function subtitle($title) {
	$url = parse_url(ROOT);
	// if($_SERVER["SCRIPT_NAME"] == $url['path']."/admin.php") { $title=  "Administration";}
	if($_SERVER["SCRIPT_NAME"] == $url['path'].'/search.php') { $title=  $_GET['requete'];}
	if($_SERVER['REQUEST_URI'] == $url['path'].'/?page='.$_GET['page']||$_SERVER['REQUEST_URI'] == $url['path'].'/index.php?page='.$_GET['page']) { $title=  '['.$_GET['page'].']';}
	if(empty($title) or !isset($title)) {$title= ' ';}
	return $title;
}
function texte_resume($texte, $nbreCar) { #Source http://www.media-camp.fr/blog/developpement/tronquer-couper-texte-php && http://fr2.php.net/manual/fr/function.str-word-count.php#73604
	$LongueurTexteBrutSansHtml = str_word_count(strip_tags($texte));
	if($LongueurTexteBrutSansHtml < $nbreCar) {
		return $texte;
	}
	else {
		$MasqueHtmlSplit = '#</?([a-zA-Z1-6]+)(?: +[a-zA-Z]+="[^"]*")*( ?/)?>#';
		$MasqueHtmlMatch = '#<(?:/([a-zA-Z1-6]+)|([a-zA-Z1-6]+)(?: +[a-zA-Z]+="[^"]*")*( ?/)?)>#';
		$texte .= ' ';
		$BoutsTexte = preg_split($MasqueHtmlSplit, $texte, -1,  PREG_SPLIT_OFFSET_CAPTURE | PREG_SPLIT_NO_EMPTY);
		$NombreBouts = count($BoutsTexte);
		if($NombreBouts == 1) {
			$longueur = str_word_count($texte);
			return substr($texte, 0, strpos($texte, ' ', $longueur > $nbreCar ? $nbreCar : $longueur));
		}
		$longueur = 0;
		$indexDernierBout = $NombreBouts - 1;
		$position = $BoutsTexte[$indexDernierBout][1] + str_word_count($BoutsTexte[$indexDernierBout][0]) - 1;
		$indexBout = $indexDernierBout;
		$rechercheEspace = true;
		foreach( $BoutsTexte as $index => $bout ) {
			$longueur += str_word_count($bout[0]);
			if($longueur >= $nbreCar) {
				$position_fin_bout = $bout[1] + str_word_count($bout[0]) - 1;
				$position = $position_fin_bout - ($longueur - $nbreCar);
				if(($positionEspace = strpos($bout[0], ' ', $position - $bout[1])) !== false ) {
					$position = $bout[1] + $positionEspace;
					$rechercheEspace = false;
				}
				if($index != $indexDernierBout) {
					$indexBout = $index + 1;
					break;
				}
			}
		}
		if($rechercheEspace === true) {
			for($i=$indexBout; $i<=$indexDernierBout; ++$i) {
				$position = $BoutsTexte[$i][1];
				if(($positionEspace = strpos($BoutsTexte[$i][0], ' ')) !== false) {
					$position += $positionEspace;
					break;
				}
			}
		}
		$texte = substr($texte, 0, $position);
		preg_match_all($MasqueHtmlMatch, $texte, $retour, PREG_OFFSET_CAPTURE);
		$BoutsTag = array();
		foreach($retour[0] as $index => $tag) {
			if(isset($retour[3][$index][0])) {
				continue;
			}

		if($retour[0][$index][0][1] !='/') {
			array_unshift($BoutsTag, $retour[2][$index][0]);
		}
		else {
			array_shift($BoutsTag);
		}
	}

		if(!empty($BoutsTag)) {
			foreach( $BoutsTag as $tag ) {
				$texte .= '</' . $tag . '>';
			}
		}
		if($LongueurTexteBrutSansHtml > $nbreCar) {
			$texte .= ' [&#8230;]';
			$texte =  str_replace('</p> [&#8230;]', '… </p>', $texte);
			$texte =  str_replace('</ul> [&#8230;]', '… </ul>', $texte);
			$texte =  str_replace('</div> [&#8230;]', '… </div>', $texte);
		}
		return $texte;
	}
}
function mylog($post, $file="log") {
	error_log(date('Y/m/d/h/i/s',time()).' - '.$post.PHP_EOL, 3, 'app/cache/log/'.$file.'.log');
}
function scandir_list_dir($name, $dirname, $selectvar) {
	echo '<select name="'.$name.'">'.PHP_EOL;
	$scandir = scandir($dirname);
	foreach($scandir as $file) {
		$hiddendir = substr($file, 0, 1);
		if($file != '.' && $file != '..' && is_dir($dirname.$file) && $hiddendir !='.') {
			$arr_file = explode('.',$file);
			$select = '';
			if($arr_file['0'] == $selectvar) {$select='selected'; }
				echo '<option value="'.$arr_file['0'].'" '.$select.'>'.$arr_file['0'].'</option>'.PHP_EOL;
		}
	}
	echo '</select><br>'.PHP_EOL;
}

function scandir_list_file($name, $dirname, $selectvar) {
	echo '<select name="'.$name.'">'.PHP_EOL;
	$scandir = scandir($dirname);
	foreach($scandir as $file) {
		if($file != '.' && $file != '..' && !is_dir($dirname.$file)) {
			$arr_file = explode('.',$file);
			$select = '';
			if($arr_file['0'] == $selectvar) {$select='selected'; }
				echo '<option value="'.$arr_file['0'].'" '.$select.'>'.$arr_file['0'].'</option>';
		}
	}
	echo '</select><br>'.PHP_EOL;
}
function date_human_posix($chaine) {
	$jour = substr($chaine, 0, 2);
	$mois = substr($chaine, 3, 2);
	$annee = substr($chaine, 6, 4);
	$heure = substr($chaine, 11, 2);
	$min = substr($chaine, 14, 2);
	$sec = substr($chaine, 17, 2);
	return mktime( $heure, $min,$sec, $mois, $jour, $annee);
}
function check_date_format($chaine) {
	if(preg_match("#[0-9]{2}/[0-9]{2}/[0-9]{4}/[0-9]{2}/[0-9]{2}/[0-9]{2}#si",$chaine)) {
		return true;
	}
	else {
		return false;
	}
}
function format_date($format, $time, $hour=false) { #source : https://en.wikipedia.org/wiki/Date_format_by_country
	switch ($format) {
		case 'iso8601': return date('c', $time); break;
		case 'yyyy-mm-dd': if($hour) {return date('Y-m-d', $time);} else {return date('Y-m-d', $time);} break;
		case 'yyyy/mm/dd': if($hour) {return date('Y/m/d/H/i/s', $time);} else {return date('Y/m/d', $time);} break;
		case 'dd-mm-yyyy': if($hour) {return date('d-m-Y H:i', $time);} else {return date('d-m-Y', $time);} break;
		case 'dd/mm/yyyy': if($hour) {return date('d/m/Y H:i', $time);} else {return date('d/m/Y', $time);} break;
		case 'dd/mm/yy': if($hour) {return date('d/m/y H:i', $time);} else {return date('d/m/y', $time);} break;
		case 'dd.mm.yyyy': if($hour) {return date('d.m.Y H:i', $time);} else {return date('d.m.Y', $time);} break;
		case 'mm/dd/yyyy': if($hour) {return date('m/d/Y H:i', $time);} else {return date('m/d/Y', $time);} break;
		case 'american': if($hour) {return date('l, F j Y H:i', $time);} else {return date('l, F jS', $time);} break;
		case 'british': if($hour) {return date('l jS F g:i A', $time);} else {return date('l, F jS', $time);} break;
		case 'i18n': if($hour) {return strftime('%A %d %B %Y - %Hh%M', $time);} else {return strftime('%A %d %B %Y', $time);} break;
		default : return date('d-m-Y H:i', $time); break;
	}
}
function form_format_date() {
	$array = array(
		'iso8601',
		'yyyy-mm-dd',
		'yyyy/mm/dd',
		'dd-mm-yyyy',
		'dd/mm/yyyy',
		'dd/mm/yy',
		'dd.mm.yyyy',
		'mm/dd/yyyy',
		'american',
		'british',
		'i18n'
	);
	$selected = '';
	$form= '<select name="format_date">'.PHP_EOL;
	foreach($array as $option) {
		if($option === FORMAT_DATE) {
			$selected = ' selected="selected"';
		}
		$form .= "\t".'<option value="'.$option.'"'.$selected.'>'.format_date($option, time(), true).'</option>'.PHP_EOL;
		$selected='';
	}
	$form .= '</select>'.PHP_EOL;
	return $form;
}
function form_timezone() { #src= http://lehollandaisvolant.net/tuto/php/#creer-une-liste-des-fuseaux-horaires
   $cache = 'app/cache/tmp/admin/timezone.html';
	if(check_cache($cache)) {readfile($cache);}
	else {
	ob_start();
		$all_timezones = timezone_identifiers_list();
		$liste_fuseau = array();
		foreach($all_timezones as $tz) {
			$spos = strpos($tz, '/');
			if ($spos !== FALSE) {
				$continent = substr($tz, 0, $spos);
				$city = substr($tz, $spos+1);
				$liste_fuseau[$continent][] = array('tz_name' => $tz, 'city' => $city);
			}
			if ($tz == 'UTC') {
				$liste_fuseau['UTC'][] = array('tz_name' => 'UTC', 'city' => 'UTC');
			}
	   }
		$form = '<p>';
		$form .= '<label for="timezone">timezone :</label>';
		$form .= '<select id="timezone" name="timezone">' ;
		foreach ($liste_fuseau as $continent => $zone) {
			$form .= '<optgroup label="'.ucfirst(strtolower($continent)).'">'.PHP_EOL;
			foreach ($zone as $fuseau) {
				if(TIMEZONE == $fuseau['tz_name']) { $selected = "selected"; } else {$selected =""; }
				$form .= '<option value="'.htmlentities($fuseau['tz_name']).'" '.$selected.' ';
				$timeoffset = date_offset_get(date_create('now', timezone_open($fuseau['tz_name'])) );
				$formated_toffset = '(UTC'.(($timeoffset < 0) ? '–' : '+').str_pad(floor((abs($timeoffset) / 3600)), 2, '0', STR_PAD_LEFT).':'.str_pad( floor((abs($timeoffset) % 3600) / 60), 2, '0', STR_PAD_LEFT).') ';
				$form .= '>'.htmlentities($fuseau['city']).' '.$formated_toffset.'</option>'.PHP_EOL;
			}
			$form .= '</optgroup>'.PHP_EOL;
		}
		$form .= '</select> '.PHP_EOL;
		$form .= '</p>'.PHP_EOL;
		echo $form;
		$page = ob_get_contents();
		ob_end_clean();
		file_put_contents($cache, $page);
		echo $page;
	}
}
function redirect($url) {
	// echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	header('Location: '.$url);
	die;
}
// ------------------------------------------------------------------------------------------
// Brute force protection system by sebsauvage.net
// Several consecutive failed logins will ban the IP address for 30 minutes.
if(!is_dir('app/cache/log')) { mkdir('app/cache/log/');}
if (!is_file('app/cache/log/ipbans.php')) { file_put_contents('app/cache/log/ipbans.php', "<?php\n\$GLOBALS['IPBANS']=".var_export(array('FAILURES'=>array(),'BANS'=>array()),true).";\n?>"); } else {include 'app/cache/log/ipbans.php';}
// Signal a failed login. Will ban the IP if too many failures:
function ban_loginFailed() {
	$ip=$_SERVER["REMOTE_ADDR"]; $gb=$GLOBALS['IPBANS'];
	if (!isset($gb['FAILURES'][$ip])) $gb['FAILURES'][$ip]=0;
	++$gb['FAILURES'][$ip];
	if ($gb['FAILURES'][$ip]>(5-1)) {
		$gb['BANS'][$ip]=time()+1800;
		mylog('IP address ('.$ip.') banned from login');
	}
	$GLOBALS['IPBANS'] = $gb;
	file_put_contents('app/cache/log/ipbans.php', "<?php\n\$GLOBALS['IPBANS']=".var_export($gb,true).";\n?>");
}

// Signals a successful login. Resets failed login counter.
function ban_loginOk() {
	$ip=$_SERVER["REMOTE_ADDR"]; $gb=$GLOBALS['IPBANS'];
	unset($gb['FAILURES'][$ip]); unset($gb['BANS'][$ip]);
	$GLOBALS['IPBANS'] = $gb;
	// mylog("login successful (".$ip.")");
	file_put_contents('app/cache/log/ipbans.php', "<?php\n\$GLOBALS['IPBANS']=".var_export($gb,true).";\n?>");
}

// Checks if the user CAN login. If 'true', the user can try to login.
function ban_canLogin() {
	$ip=$_SERVER["REMOTE_ADDR"]; $gb=$GLOBALS['IPBANS'];
	if (isset($gb['BANS'][$ip])) {
		// User is banned. Check if the ban has expired:
		if ($gb['BANS'][$ip]<=time()) { // Ban expired, user can try to login again.
			mylog('Ban lifted for ('.$ip.').');
			unset($gb['FAILURES'][$ip]); unset($gb['BANS'][$ip]);
			file_put_contents('app/cache/log/ipbans.php', "<?php\n\$GLOBALS['IPBANS']=".var_export($gb,true).";\n?>");
			return true; // Ban has expired, user can login.
		}
		return false; // User is banned.
	}
	return true; // User is not banned.
}
function check_cache($file) {
	if(file_exists($file) && filemtime($file) >time()-3600 && DEBUG == false && ENABLE_CACHE == true) {
		return true;
	}
	else {
		return false;
	}
}
function send_mail($to, $from, $subject, $message, $addheaders=null) {
	 $headers = 'From: '.$from.PHP_EOL.'Reply-To: '.$from.PHP_EOL.'X-Mailer: PHP/'.phpversion().PHP_EOL.'Content-Type: text/plain; charset="utf-8"; format="fixed"';
	 $headers .= $addheaders;
	 mail($to, $subject, $message, $headers);
}
function erased_dir($dir) {
	if(!is_dir($dir)) {
		throw new InvalidArgumentException('$dir must be a directory');
	}
	if(substr($dir,strlen($dir)-1,1) != '/') {
		$dir .= '/';
	}
	$files = glob($dir.'*',GLOB_MARK);
	if($files) {
		foreach ($files as $file) {
			if(is_dir($file)) {
				erased_dir($file);
			} 
			else {
				unlink($file);
			}
		}
	}
	rmdir($dir);
}
function delete_file($url) {
	if(file_exists($url)) {
		unlink($url);
	}
}
function Zip($source, $destination) {
	if(!extension_loaded('zip') || !file_exists($source)) {
		return false;
	}

	$zip = new ZipArchive();
	if(!$zip->open($destination, ZIPARCHIVE::CREATE)) {
		return false;
	}
	$source = str_replace('\\', '/', realpath($source));
	if(is_dir($source) === true) {
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

		foreach($files as $file) {
			$file = str_replace('\\', '/', $file);
			// Ignore "." and ".." folders
			if(in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
			continue;
			$file = realpath($file);
			if(is_dir($file) === true) {
				$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
			}
			else if (is_file($file) === true) {
				$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
			}
		}
	}
	else if (is_file($source) === true) {
		$zip->addFromString(basename($source), file_get_contents($source));
	}
	return $zip->close();
}
function backup_mysql() {
	global $bdd;
	$f = fopen('data/backup-'.date('Y-m-d', time()).'.sql', 'wt' );
	$tables = $bdd->query('SHOW TABLES');
	foreach ( $tables as $table) {
		echo $table[0] . ' ... '; flush();
		$sql = '-- TABLE: ' . $table[0] . PHP_EOL;
		$create = $bdd->query('SHOW CREATE TABLE `' . $table[0] . '`')->fetch();
		$sql .= $create['Create Table'] . ';' . PHP_EOL;
		fwrite( $f, $sql );

		$rows = $bdd->query( 'SELECT * FROM `' . $table[0] . '`' );
		$rows->setFetchMode( PDO::FETCH_ASSOC );
		foreach ( $rows as $row ) {
			$row = array_map( array( $bdd, 'quote' ), $row );
			$sql = 'INSERT INTO `' . $table[0] . '` (`' . implode( '`, `', array_keys( $row ) ) . '`) VALUES (' . implode( ', ', $row ) . ');' . PHP_EOL;
			fwrite( $f, $sql );
		}

		$sql = PHP_EOL;
		$result = fwrite( $f, $sql );
		flush();
	}
	fclose($f);
		return 'data/backup-'.date('Y-m-d', time()).'.sql';
}
function getSslPage($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
function update_lib($file, $lib) {
	if(file_exists($file)) {
		if (strcmp(file_get_contents($file), getSslPage($lib)) !== 0) {
			file_put_contents($file, getSslPage($lib));
		}
		else {
		}
	}
	else {
		file_put_contents($file, getSslPage($lib));
	}
}
function get_title_url($link) {
	$html = new DOMDocument();
	@$html->loadHtmlFile($link);
	$xpath = new DOMXPath($html);
	$domExemple = $xpath->query("//title");
	$i = 0;
	foreach ($domExemple as $exemple) {
		$result[$i] = $exemple->nodeValue;
		$i++;
	}
	return $result[0];
}
############### SQL Functions ###############
function sql($query, $array=array()) {
	global $bdd;
	$retour = $bdd->prepare($query);
	$retour->execute($array);
	debug('query');
}
function push_pshb() {
	$p = new Publisher('http://pubsubhubbub.appspot.com');
	$topic_url = ROOT;
	if ($p->publish_update($topic_url)) {
		mylog('OK', 'pshb');
	}
	else {
		mylog('BAD'.print_r($p->last_response()), 'pshb');
	}
}
?>